import { AuthConstants } from './../../config/auth-constants';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { StorageService } from '../../services/storage.service';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public postData = {
      username: '',
      password: ''
  };

  constructor(
    private router: Router, 
    private authService: AuthService,
    private storageService: StorageService,
    private toastService: ToastService
  ) { }

  ngOnInit() {
  }

  validateInputs(){
    let username = this.postData.username.trim();
    let password = this.postData.password.trim();

    return ( 
      this.postData.username && 
      this.postData.password && 
      username.length > 0 && 
      password. length > 0
    );
  }

  loginAction() {
    // this.router.navigate(['/home/feed'])
    //console.log(this.postData);
    if(this.validateInputs()){
      this.authService.login(this.postData).subscribe((res: any) => {
        if(res.userData) {
          this.storageService.store(AuthConstants.Auth, res.userData);
          this.router.navigate(['home']);
        } else {
          // console.log("Incorrect informations");
          this.toastService.presentToast("Incorrect informations");
        }
      },
        (error: any) => {
          this.toastService.presentToast("Network Connection Error");
        }
      )
    } else {
      this.toastService.presentToast("Please Give some informations");
    }
  }

}
