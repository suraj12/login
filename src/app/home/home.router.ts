import { UserDataResolver } from './../resolver/userData.resolver';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';
import { HomeGuard } from '../guards/home.guard';

const routes: Routes = [
  {
    path: 'home',
    component: HomePage,
    canActivate: [HomeGuard],
    resolve: {
        userData: UserDataResolver
    },
    children: [
        {
            path: 'feed',
            loadChildren:() => 
                import('../pages/feed/feed.module').then(
                    m => m.FeedPageModule
                )
        },
        {
            path: 'notification',
            loadChildren:() => 
                import('../pages/notification/notification.module').then(
                    m => m.NotificationPageModule
                )
        },
        {
            path: 'messages',
            loadChildren:() => 
                import('../pages/messages/messages.module').then(
                    m => m.MessagesPageModule
                )
        },
        {
            path: 'settings',
            loadChildren:() => 
                import('../pages/settings/settings.module').then(
                    m => m.SettingsPageModule
                )
        },
        {
            path: '',
            redirectTo: '/home/feed',
            pathMatch: 'full'
        }

    ]
  }];

  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class HomeRouter {}