import { AuthConstants } from './../config/auth-constants';
import { Router } from '@angular/router';
import { HttpService } from './http.service';
import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userData$ = new BehaviorSubject<any>('');

  constructor(
    private http: HttpService, 
    private storageService: StorageService, 
    private router: Router
  ) { }


  getUserData() {
    this.storageService.get(AuthConstants.Auth).then(res => {
      console.log(res);
      this.userData$.next(res);
    });
  }

  login(postData: any): Observable<any> {
    return this.http.post('login', postData);
  }

  signup(postData: any): Observable<any> {
    return this.http.post('signup', postData);
  }

  logout() {
    //this.storageService.clear;
    this.storageService.removeItem(AuthConstants.Auth).then(res => {
      this.userData$.next('');
      this.router.navigate(['']);
    });
  }
}
