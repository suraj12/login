import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { headersToString } from 'selenium-webdriver/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private htttp: HttpClient) { }

  post(serviceName: string, data: any) {

    const headers = new HttpHeaders();
    const options = { headers: headers, withCredintials: false};

    const url = environment.apiUrl + serviceName;

    return this.htttp.post(url, JSON.stringify(data), options);

  }
}
